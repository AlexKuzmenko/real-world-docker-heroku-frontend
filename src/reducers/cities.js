const citiesReducer = (state, action) => {
    switch (action.type) {
        case 'POPULATE_CITIES':
            return action.cities
        case 'ADD_CITY':
            let id = (state.length > 0 ) ? state[state.length - 1].id + 1 : 0;
            return [
                ...state,
                { id: id, name: action.name }
            ]
        case 'REMOVE_CITY':
            return state.filter((city) => city.id !== action.id )
        default:
            return state
    }
}

export default citiesReducer;