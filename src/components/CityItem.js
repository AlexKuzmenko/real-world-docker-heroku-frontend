import React, {useEffect, useContext, useState} from 'react';
import {ListGroupItem} from "react-bootstrap";
import WeatherAppContext from "../context/weather-app-context";

import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faTrash } from '@fortawesome/free-solid-svg-icons';

const CityItem = ({ city }) => {
    const {dispatchCities} = useContext(WeatherAppContext);
    const {setCurrentCity} = useContext(WeatherAppContext);

    const clickHandle = (e) => {
        e.preventDefault();
        setCurrentCity(e.target.dataset.city);
    }

    return (
        <ListGroupItem className='d-flex justify-content-between align-items-center'>
            <div>
                <h4>
                    <a href="#" onClick={clickHandle} data-city={city.name}>
                        {city.name}
                    </a>

                </h4>
            </div>
            <div>
                <FontAwesomeIcon onClick={() => dispatchCities({type: "REMOVE_CITY", id: city.id})} icon={faTrash} />
            </div>
        </ListGroupItem>
    )
}

export default CityItem;