import React, {useEffect, useReducer, useState} from 'react';
import CityList from "./CityList";
import AddCityForm from "./AddCityForm";
import WeatherView from "./WeatherView";

import citiesReducer from "../reducers/cities";
import weatherReducer from "../reducers/weather";

import WeatherAppContext from "../context/weather-app-context";

import {Container, Row, Col} from "react-bootstrap";


const WeatherApp = () => {
    const [cities, dispatchCities] = useReducer(citiesReducer, [])
    const [weather, dispatchWeather] = useReducer(weatherReducer, null);
    const [currentCity, setCurrentCity] = useState('')

    useEffect(() => {
        const cityData = JSON.parse(localStorage.getItem('cities'))
        if (cityData) {
            console.log(cityData);
            dispatchCities({ type: 'POPULATE_CITIES', cities: cityData })
        }
    }, [])
    useEffect(() => {
        localStorage.setItem('cities', JSON.stringify(cities))
    }, [cities])

    useEffect(() => {
        if (currentCity.length > 0) {
            fetchWeather();
        }

    }, [currentCity]);

    const fetchWeather = async () => {
        let appId = 'b5018676b6c9e7d01aa7056fd2b9186d'
        let url = `https://api.openweathermap.org/data/2.5/weather?q=${currentCity}&appid=${appId}`
        let result = await fetch(url)
        let data = await result.json();
        if (data.cod == 200) {
            console.log(data);
            dispatchWeather({type: 'POPULATE_WEATHER', data: data});
        }
    }

    return (
        <WeatherAppContext.Provider value={{cities, dispatchCities, weather, dispatchWeather, currentCity, setCurrentCity}}>
            <Container className="border border-4 p-4">
                <h1>Weather Application</h1>
                <Row>
                    <Col xs={12} md={6} lg={4}>
                        <AddCityForm />
                        <CityList />
                    </Col>
                    <Col xs={12} md={6} lg={8}>
                        <WeatherView className="border border-2 p-3" />
                    </Col>
                </Row>
            </Container>
        </WeatherAppContext.Provider>
    )
}

export default WeatherApp;