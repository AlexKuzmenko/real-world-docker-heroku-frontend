import React, {useState, useContext} from "react";
import WeatherAppContext from "../context/weather-app-context";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Row, Col} from "react-bootstrap";

const AddCityForm = () => {
    const {dispatchCities} = useContext(WeatherAppContext);
    const [name, setName] = useState('')

    const addCity = (e) => {
        e.preventDefault()
        dispatchCities({
            type: 'ADD_CITY',
            name
        })
        setName('')
    }

    return (
        <div className="border border-2 p-3 mb-3">
            <h3>Add city</h3>
            <Form onSubmit={addCity} className="mb-3">
                <Row>
                    <Col xs={12}>
                        <Form.Group className="mb-3" controlId="formCityName">
                            <Form.Control placeholder='City Name' value={name} onChange={(e) => setName(e.target.value)} />
                        </Form.Group>
                    </Col>
                    <Col xs={12} className='text-start'>
                        <Button variant="primary" type="submit">
                            Submit
                        </Button>
                    </Col>
                </Row>


            </Form>
        </div>
    )
}

export default AddCityForm;