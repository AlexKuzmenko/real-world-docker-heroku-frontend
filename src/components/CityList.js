import CityItem from "./CityItem";
import {ListGroup} from "react-bootstrap";
import React, {useContext} from "react";
import WeatherAppContext from "../context/weather-app-context";

const CityList = () => {
    const {cities} = useContext(WeatherAppContext)
    return (
        <div className="border border-2 p-3">
            <h3>City List</h3>
            <ListGroup>
                {cities.map((city) => (
                    <CityItem key={city.id} city={city} />
                ))}
            </ListGroup>
        </div>

    )
}

export default CityList;